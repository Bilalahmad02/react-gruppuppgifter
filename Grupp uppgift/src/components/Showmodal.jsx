import React, { useState } from 'react';
import UserData from "./memberdata.json";
import Styles from './modal.module.css';

export const Showmodal = ({ editPopup, selectedMemberIndex, cancelEditbtn, updateMemberState}) => {
  const selectedMember = UserData.members[selectedMemberIndex];
  const [updatedName, setUpdatedName] = useState(selectedMember?.name || '');
  const [updatedCity, setUpdatedCity] = useState(selectedMember?.city || '');

  const updateName = (e) => {
    setUpdatedName(e.target.value);
  };

  const updateCity = (e) => {
    setUpdatedCity(e.target.value);
  };

  const updateUser = () => {
    const updatedMembers = [...UserData.members];
    updatedMembers[selectedMemberIndex] = {
      ...selectedMember,
      name: updatedName,
      city: updatedCity,
    };

    // Use the callback function to update the state in the main page
    updateMemberState(updatedMembers);

    // Reset the updatedName and updatedCity states
    setUpdatedName('');
    setUpdatedCity('');

    // Close the modal or perform other actions
    cancelEditbtn();
  };

  return (
    <>
      {editPopup && (
        <div className={Styles.container}>
          <div className={Styles.input}>
            <div className={Styles.userName}>
              <input type="text" placeholder='Name' value={updatedName} onChange={updateName} />
            </div>
            <div className={Styles.city}>
              <input type="text" placeholder='City' value={updatedCity} onChange={updateCity} />
            </div>
          </div>
          <div className={Styles.btns}>
            <div className={Styles.cancelbtn}>
              <button onClick={cancelEditbtn}>Cancel</button>
            </div>
            <div className={Styles.updatebtn}>
              <button onClick={updateUser}>Update</button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
