import React, { useState } from 'react'
import Styles from './membercard.module.css'
import { FaUserEdit } from "react-icons/fa";

export default function Memebercards({imgsrc, imgalt, name, city, description, index, showEditModal}) {

  return (
    <div className={Styles.memberCardContainer}>
        <div className={Styles.card}>
            <div className={Styles.imgContainer}>
                <img src={imgsrc} alt={imgalt}/>
            </div>
            <div className={Styles.memberinfo}>
                <div className={Styles.namencity}>
                    <div className={Styles.userIcon}>
                        <p>{name}</p>
                        <p>{city}</p>
                    </div>
                <div className={Styles.editicon}>
                    <FaUserEdit title='edit member info' onClick={()=>showEditModal(index)}/>
                </div>
                </div>
                <div className={Styles.description}>
                    <p>{description}</p>
                </div>
            </div>
        </div>
    </div>
  )
}
