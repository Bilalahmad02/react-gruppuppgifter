import Memebercards from "./components/Memebercards";
import UserData from "./components/memberdata.json";
import React, {useState} from 'react'
import { Showmodal } from "./components/Showmodal";
import "./App.css";

function App() {
  const [searchInputValue, setSearchInputValue] = useState('')
  const [selectedMemberIndex, setSelectedMemberIndex] = useState(null);
  const [editPopup, setEditPopup] = useState(false)
  const [memberState, setMemberState] = useState(UserData.members)

  const updateMemberState = (updatedMembers) => {
    console.log('Updating member state:', updatedMembers);
    setMemberState(updatedMembers);
  };


    const  showModal = (index) => {
      setSelectedMemberIndex(index);
      setEditPopup(!editPopup);
    };

  const  handleChange = (e) =>{
    setSearchInputValue(e.target.value);
  }

  const cancelEditbtn = () => {
    setEditPopup(!editPopup);
  }

  const filterMembers = () => {
    return memberState.filter((member) => {
      const memberName = member.name.toLowerCase();
      const memberCity = member.city.toLowerCase();
      const searchValue = searchInputValue.toLowerCase();

      return memberName.includes(searchValue) || memberCity.includes(searchValue);
    });
  };
  return (
    <>
    <div className="searchbar--container">
      <input type="text" placeholder="Search..." value={searchInputValue} onChange= {handleChange}/>
    </div>
      <div className="cards">
        {memberState &&
          filterMembers().map((member, index) => (
            <Memebercards
              key={index}
              index={index}
              imgsrc={member.img}
              name={member.name}
              city={member.city}
              description={member.description}
              showEditModal={showModal}
            />
          ))}
    </div>
    <Showmodal editPopup={editPopup} selectedMemberIndex={selectedMemberIndex} cancelEditbtn={cancelEditbtn} updateMemberState={updateMemberState}/>
    </>
  );
}

export default App;
